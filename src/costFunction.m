function [J, grad] = costFunction(theta, X, y)
%COSTFUNCTION Compute cost and gradient for logistic regression
%   J = COSTFUNCTION(theta, X, y) computes the cost of using theta as the
%   parameter for logistic regression and the gradient of the cost
%   w.r.t. to the parameters.

% Initialize some useful values
m = length(y); % number of training examples
% You need to return the following variables correctly 
J = 0;

for i = 1:m
    J += computeCost(X(i, :), y(i), theta)/m;
end

grad = computeGradient(X, y, theta, m);

end

function J = computeCost(x, y, theta)
    z = x * theta;
    J = -1 * y * log(sigmoid(z)) - (1 - y) * log(1 - sigmoid(z));
end

function grad = computeGradient(X, y, theta)
    m = length(y);
    grad = zeros(size(theta));
    n = length(grad);

    for j = 1:n
        for i = 1:m
            x = X(i, :); z = x * theta;
            grad(j) += ( (sigmoid(z) - y(i)) * x(j) )/m;
        end
    end
end
