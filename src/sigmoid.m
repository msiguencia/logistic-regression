function g = sigmoid(z)
%SIGMOID Compute sigmoid functoon
%   J = SIGMOID(z) computes the sigmoid of z.

% You need to return the following variables correctly
% The sigmoid function should be computed for each value of z, where z can be a
% matrix, a vector, or a scalar.
g = zeros(size(z));

[m, n] = size(z);

for i = 1:m
    for j = 1:n
        g(i, j) += 1/(1 + exp(-1 * z(i, j)));
    end
end

end
